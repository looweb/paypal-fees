import { Component, EventEmitter, Output, OnInit } from '@angular/core';
import { RATES } from '../mock-amount';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-fees',
  templateUrl: './fees.component.html',
  styleUrls: ['./fees.component.scss']
})
export class FeesComponent implements OnInit {

  constructor(private http: HttpClient, private route: ActivatedRoute) { }

  @Output() newPercent = new EventEmitter<any>();
  @Output() newDollar = new EventEmitter<any>();

  percent: any = RATES.percent;
  fixed: any = RATES.fixed;
  dollar: any;
  isLoading: boolean = true;
  queryParams: Array<Object> = [];

  ngOnInit() {
    this.http.get('https://s3.amazonaws.com/dolartoday/data.json')
      .subscribe((data) => {
        this.dollar = Number(data['USD']['dolartoday']);
        this.newDollar.emit(this.dollar);
        

        // Catch params if exist
        this.route.queryParams.subscribe(params => {
          const keys = Object.keys(params);
          for (let i = 0; i < keys.length; i += 1) {
            const key = keys[i];
            this.queryParams[i] = { key, value: params[key] };
          }
          this.dollar = params.dolar || this.dollar;
          this.percent = params.comision || this.percent;

          this.newPercent.emit(this.percent);
          this.newDollar.emit(this.dollar);

          this.isLoading = false;
        });
    });
  }

  changePercent(event) {
    this.percent = event.target.value;
    this.newPercent.emit(this.percent);
  }

  changeDollar(event) {
    this.dollar = event.target.value;
    this.newDollar.emit(this.dollar);
  }

}
