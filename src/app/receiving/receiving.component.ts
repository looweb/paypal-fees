import { Component, OnInit, HostListener } from '@angular/core';
import { RATES } from '../mock-amount';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-receiving',
  templateUrl: './receiving.component.html',
  styleUrls: ['./receiving.component.scss']
})

export class ReceivingComponent implements OnInit {

  constructor(private route: ActivatedRoute) {}

  sectionTitle = "Comisiones para recibir";
  sent = 0;
  fee: any = 0;
  total: any = 0;
  paypalPercent: any = RATES.percent;
  paypalFee: any = RATES.fixed;
  isMobile: boolean = false;
  dollar: any;
  totalBs: any = 0;

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.isMobile = window.innerWidth <= 600;
  }

  ngOnInit() {
    this.isMobile = window.innerWidth <= 600;
    this.route.queryParams.subscribe(params => {
      this.sent = params.monto || this.sent;
    });
    
  }

  setSent(sent: any) {
    this.sent = sent;
  }

  setPercent(percent: any) {
    this.paypalPercent = percent;
  }

  setDollar(newDollar) {
    this.dollar = newDollar;
  }

  calcReceiving() {
    this.total = (Number(this.sent) + Number(this.paypalFee)) / (1 - Number((Number(this.paypalPercent)/100).toFixed(4)));
    this.fee = (Number(this.paypalPercent) * Number(this.total) / 100) + Number(this.paypalFee);
    this.total = !!this.sent ? this.total.toFixed(2) : 0;
    this.fee = !!this.sent ? this.fee.toFixed(2) : 0;
  }

  calcBs() {
    this.totalBs = Number((this.dollar * this.sent).toFixed(2)).toLocaleString('es-VE');
  }

  calcReceivingFromSent(event) {
    this.setSent(event.target.value);
    this.calcReceiving();
  }

  calcReceivingFromFee(newPercent) {
    this.setPercent(newPercent);
    this.calcReceiving();
  }

  calcBsFromDollar(newDollar) {
    this.setDollar(newDollar);
    this.calcBs();
  }

  calcBsFromSent(event) {
    this.setSent(event.target.value);
    this.calcBs();
  }

  calcBsFromFee(newPercent) {
    this.calcReceivingFromFee(newPercent);
    this.calcBs();
  }

}
