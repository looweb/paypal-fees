import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { SendingComponent } from './sending/sending.component';
import { ReceivingComponent } from './receiving/receiving.component';
import { FeesComponent } from './fees/fees.component';
import { HttpClientModule } from '@angular/common/http';
import { UrlComponent } from './url/url.component';
import { ClipboardModule } from 'ngx-clipboard';
import { MessageComponent } from './message/message.component';

@NgModule({
  declarations: [
    AppComponent,
    SendingComponent,
    ReceivingComponent,
    FeesComponent,
    UrlComponent,
    MessageComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FontAwesomeModule,
    HttpClientModule,
    ClipboardModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
