import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss']
})
export class MessageComponent implements OnInit {

  constructor() { }

  @Input() message: Object;
  type: string = 'info';
  text: string = 'Mensaje';

  ngOnInit() {
    this.type = this.message['type'];
    this.text = this.message['text'];
  }

}
