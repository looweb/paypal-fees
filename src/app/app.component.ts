import { Component, OnInit, HostListener } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  constructor(private http: HttpClient) { }

  appTitle = "Calculadora PayPal";
  isMobile: boolean = false;
  showAlert: boolean = false;
  visits: any = 0;
  msg: Object;

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.isMobile = window.innerWidth <= 750;
  }

  ngOnInit() {
    this.isMobile = window.innerWidth <= 750;
    this.http.get('https://api.ipify.org/', {responseType: 'text'})
      .subscribe(
        data => {
          this.http.post('http://localhost/angular/paypalfees-api/paypalfees.php', {ip: data})
            .subscribe(
              data => {
                this.visits = Number(data['total']).toLocaleString('es-VE');
              },
              error => {
                console.log(error);
              }
            )
        },
        error => {
          console.log(error);
        });
  }

  showMessage(message) {
    this.showAlert = false;
    this.showAlert = true;
    this.msg = message;
    setTimeout(() => {
      this.showAlert = false;
    }, 3000);
  }
}
