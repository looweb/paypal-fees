import { Component, EventEmitter, Output, Input } from '@angular/core';

@Component({
  selector: 'app-url',
  templateUrl: './url.component.html',
  styleUrls: ['./url.component.scss']
})
export class UrlComponent {

  constructor() { }

  @Output() showMessage = new EventEmitter<Object>();

  @Input() dollar: any;
  @Input() fee: any;
  @Input() amount: any;
  baseUrl: string = null;

  getUrl() {
    this.baseUrl = `${window.origin}${window.location.pathname}/?monto=${this.amount}&comision=${this.fee}&dolar=${this.dollar}`;
    const message = { type: 'success', text: 'Enlace generado!' };
    this.showMessage.emit(message);
  }

  copyUrl() {
    this.baseUrl = null;
    const message = { type: 'success', text: 'Enlace Copiado!' };
    this.showMessage.emit(message);
  }

}
