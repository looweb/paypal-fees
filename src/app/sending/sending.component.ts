import { Component, HostListener, EventEmitter, OnInit, Output } from '@angular/core';
import { RATES } from '../mock-amount';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-sending',
  templateUrl: './sending.component.html',
  styleUrls: ['./sending.component.scss']
})
export class SendingComponent implements OnInit {

  constructor(private route: ActivatedRoute) {}

  @Output() throwMessage = new EventEmitter<any>();

  sectionTitle = "Comisiones al enviar";
  sent = 0;
  fee: any = 0;
  total: any = 0;
  totalBs: any = 0;
  paypalPercent: any = RATES.percent;
  paypalFee: any = RATES.fixed;
  isMobile: boolean = false;
  dollar: any;
  urlParams: string = '';

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.isMobile = window.innerWidth <= 600;
  }

  ngOnInit() {
    this.isMobile = window.innerWidth <= 600;
    this.route.queryParams.subscribe(params => {
      this.sent = params.monto || this.sent;
    });
    
  }

  setSent(sent: any) {
    this.sent = sent;
  }

  setPercent(percent: any) {
    this.paypalPercent = percent;
  }

  setDollar(newDollar) {
    this.dollar = newDollar;
  }

  calcSending() {
    this.fee = (this.sent*this.paypalPercent/100 + this.paypalFee);
    this.fee = this.sent > 0 ? this.fee.toFixed(2) : 0;
    this.total = (this.sent - this.fee);
    this.total = this.total <= 0 ? 0 : this.total.toFixed(2);
  }

  calcBs() {
    this.totalBs = Number((this.dollar * this.total).toFixed(2)).toLocaleString('es-VE');
  }

  calcSendingFromSent(event) {
    this.setSent(event.target.value);
    this.calcSending();
  }

  calcSendingFromFee(newPercent) {
    this.setPercent(newPercent);
    this.calcSending();
  }

  calcBsFromDollar(newDollar) {
    this.setDollar(newDollar);
    this.calcBs();
  }

  calcBsFromSent(event) {
    this.setSent(event.target.value);
    this.calcBs();
  }

  calcBsFromFee(newPercent) {
    this.calcSendingFromFee(newPercent);
    this.calcBs();
  }

  showMessage(event) {
    this.throwMessage.emit(event);
  }

}
